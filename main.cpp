#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using std::cout;
using std::endl;

int main()
{
    cv::VideoCapture cap("sample_mpg.avi"); // видео
    if (!cap.isOpened())
        return -1;
    cv::Mat src1, src2; // предыдущий и текущий кадр
    // детекторы для поиска ключевых точек
    cv::Ptr<cv::FeatureDetector> detectorSURF; // детектор SURF
    detectorSURF = cv::xfeatures2d::SURF::create();
    cv::Ptr<cv::FeatureDetector> detectorSIFT; // детектор SIFT
    detectorSIFT = cv::xfeatures2d::SIFT::create();
    cv::Ptr<cv::FeatureDetector> detectorBRISK; // детектор BRISK
    detectorBRISK = cv::BRISK::create();
    // дескрипторы для описания ключевых точек
    cv::Mat descr1SURF, descr2SURF; // дескрипторы SURF
    cv::Mat descr1SIFT, descr2SIFT; // дескрипторы SIFT
    cv::Mat descr1BRISK, descr2BRISK; // дескрипторы BRISK
    // ключевые точки
    std::vector<cv::KeyPoint> keys1SURF, keys2SURF; // ключевые точки SURF
    std::vector<cv::KeyPoint> keys1SIFT, keys2SIFT; // ключевые точки SIFT
    std::vector<cv::KeyPoint> keys1BRISK, keys2BRISK; // ключевые точки BRISK
    // экстракторы для составления описания ключевых точек, хранящегося в дискрипторе
    cv::Ptr<cv::DescriptorExtractor> extractorSURF; // экстрактор SURF
    extractorSURF = cv::xfeatures2d::SURF::create();
    cv::Ptr<cv::DescriptorExtractor> extractorSIFT; // экстрактор SIFT
    extractorSIFT = cv::xfeatures2d::SIFT::create();
    cv::Ptr<cv::DescriptorExtractor> extractorBRISK; // экстрактор BRISK
    extractorBRISK = cv::BRISK::create();
    // матчеры для сопоставления ключевых точек на текущем и предыдущем кадре видео по их описанию, хранящемуся в дескрипторах
    cv::BFMatcher matcherSURF; // матчер SURF
    cv::BFMatcher matcherSIFT; // матчер SIFT
    cv::BFMatcher matcherBRISK; // матчер BRISK
    // результаты сопоставления ключевых точек матчерами
    std::vector<cv::DMatch> matchesSURF; // для SURF
    std::vector<cv::DMatch> matchesSIFT; // для SIFT
    std::vector<cv::DMatch> matchesBRISK; // для BRISK
    // изображения с сопоставленными ключевыми точками
    cv::Mat img_matchesSURF; // для SURF
    cv::Mat img_matchesSIFT; // для SIFT
    cv::Mat img_matchesBRISK; // для BRISK

    // на экран в трех окнах выводятся результаты сопоставления ключевых точек по алгоритмам SURF, SIFT и BRISK
    // остановить просмотр можно нажатием esс, при этом окна будут отображать последние кадры
    // чтобы закрыть окна отображения нужно нажать пробел
    cout << "press esc to stop calculations\npress space to close all windows\n";
    bool stop = false; // для остановки при нажатии esc
    double rate = cap.get(cv::CAP_PROP_FPS); // частота кадров видео
    int delay = 1000 / rate; // задержка для смены кадров в видео в мс
    cap >> src2; // первый кадр видео
    while (!stop)
    {
        // Проверяем доступность кадра
        bool result = cap.grab(); // проверка доступности следующего кадра видео
        if (result)
        {
            src1 = src2; // предыдущий кадр
            cap >> src2; // чтение текущего кадра
            // поиск ключевых точек на предыдущем и текущем кадрах
            detectorSURF->detect(src1, keys1SURF);
            detectorSURF->detect(src2, keys2SURF);
            detectorSIFT->detect(src1, keys1SIFT);
            detectorSIFT->detect(src2, keys2SIFT);
            detectorBRISK->detect(src1, keys1BRISK);
            detectorBRISK->detect(src2, keys2BRISK);
            // составление описания ключевых точек
            extractorSURF->compute(src1, keys1SURF, descr1SURF);
            extractorSURF->compute(src2, keys2SURF, descr2SURF);
            extractorSIFT->compute(src1, keys1SIFT, descr1SIFT);
            extractorSIFT->compute(src2, keys2SIFT, descr2SIFT);
            extractorBRISK->compute(src1, keys1BRISK, descr1BRISK);
            extractorBRISK->compute(src2, keys2BRISK, descr2BRISK);
            // сопоставление ключевых точек
            matcherSURF.match(descr1SURF, descr2SURF, matchesSURF);
            matcherSIFT.match(descr1SIFT, descr2SIFT, matchesSIFT);
            matcherBRISK.match(descr1BRISK, descr2BRISK, matchesBRISK);
            // формирование изображений по результатам сопоставления ключевых точек
            cv::drawMatches(src1, keys1SURF, src2, keys2SURF, matchesSURF, img_matchesSURF);
            cv::drawMatches(src1, keys1SIFT, src2, keys2SIFT, matchesSIFT, img_matchesSIFT);
            cv::drawMatches(src1, keys1BRISK, src2, keys2BRISK, matchesBRISK, img_matchesBRISK);
        }
        else
            continue;
        // отображение результатов
        cv::imshow("SURF", img_matchesSURF);
        cv::imshow("SIFT", img_matchesSIFT);
        cv::imshow("BRISK", img_matchesBRISK);
        int key = cv::waitKey(delay); // ожидание нажатия кнопки
        if (key==27) // если это 0x27, т.е. esc
            stop=true; // остановка расчетов и отображения результатов
    }
    int key = waitKey(); // чтобы закрыть окна, нужно нажать пробел
    if (key == 32)
        cv::destroyAllWindows();
    return 0;
}
